﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for InventoryControll.xaml
    /// </summary>
    public partial class InventoryControll : Window
    {

        SqlCommand sqlCmd;
        String query = "SELECT itemID, name FROM inventory ORDER BY itemID ASC";
        DataSet dtSet = new DataSet();
        public static String oldName;

        public InventoryControll()
        {
            InitializeComponent();
            BindData();
            itemSearchBox.Focus();
        }
        void WindowActive(object sender, EventArgs e)
        {
            // Application activated
            BindData();
        }

        void WindowNotActive(object sender, EventArgs e)
        {
            // Application deactivated
            //this.isActive = false;
        }


        private void BindData()
        {
            using (SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection())
            {
                dtSet.Clear();
                sqlCmd = new SqlCommand(query, sqlCon);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtSet, "inventory");
                ItemList.DataContext = dtSet;
            }
        }

        private void itemSearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (itemSearchBox.Text.Length < 1)
            {
                ItemList.DataContext = dtSet;
            }
            else
            {
                DataSet tempData = dtSet;
                DataSet newdataset = new DataSet();
                string searchString = "name Like '%" + itemSearchBox.Text + "%'";
                string orderString = "itemID ASC";
                newdataset.Merge(tempData.Tables[0].Select(searchString, orderString));
                ItemList.DataContext = newdataset;
            }

        }

        private void ItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView drv = (DataRowView)ItemList.SelectedItem;

            string searchID;
            if (drv != null)
            {
                searchID = drv["itemID"].ToString().Trim();

                SqlCommand searchSqlCmd;
                string searchQuery = "SELECT * from inventory WHERE itemID = " + searchID;
                using (SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection())
                {
                    if (sqlCon.State == ConnectionState.Closed)
                    {
                        sqlCon.Open();
                    }

                    searchSqlCmd = new SqlCommand(searchQuery, sqlCon);
                    using (SqlDataReader reader = searchSqlCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            UserIDBox.Text = reader["itemID"].ToString().Trim();
                            UsernameBox.Text = reader["name"].ToString().Trim();
                            PasswordBox.Text = reader["stock"].ToString().Trim();
                            hoursWorkedBox.Text = reader["price"].ToString().Trim();
                            vatBox.Text = reader["fldVAT"].ToString().Trim();

                        }

                        //Sätter värdet i vatbox rätt till .(punk) iställer för komma(,) .. Blir fel i DB annars vid update
                        if (vatBox.Text == "0,06"){vatBox.Text = "0.06";}
                        else if (vatBox.Text == "0,12") { vatBox.Text = "0.12"; }
                        else if (vatBox.Text == "0,25") { vatBox.Text = "0.25"; }
                        


                    }

                    sqlCon.Close();
                }
                oldName = UsernameBox.Text;
            }
        }


        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();
            String query = "DELETE FROM inventory Where itemID=" + "'" + UserIDBox.Text + "'";

            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }

                if (MessageBox.Show(" Are you Sure that you want to Delete the following Item from the System? " + UsernameBox.Text, "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes)

                {
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.ExecuteNonQuery();

                    MessageBox.Show("The item " + "" + UsernameBox.Text + " has successfully been Deleted");
                    BindData();
                }
                else
                {
                    return;
                }
            }

            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
            finally
            {
                sqlCon.Close();
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {

            SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();
            String query = "Update inventory set name='" + UsernameBox.Text + "',stock='" + PasswordBox.Text + "',price='" + hoursWorkedBox.Text + "',fldVAT='" + vatBox.Text + "' where itemID=" + "'" + UserIDBox.Text + "'";


            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }

                if (MessageBox.Show(" Are you Sure that you want to update item " + oldName + "?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes)

                {
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.ExecuteNonQuery();

                    MessageBox.Show("The Item " + oldName + " has successfully been Updated with new info.");
                    BindData();
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public void vatSIX_Click(object sender, RoutedEventArgs e)
        {
            vatBox.Text = "0.06";
        }

        private void twelveVAT_Click(object sender, RoutedEventArgs e)
        {
            vatBox.Text = "0.12";

        }

        private void twofiveVAT_Click(object sender, RoutedEventArgs e)
        {
            vatBox.Text = "0.25";
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow openAdmin = new AdminWindow();
            openAdmin.Show();
            this.Close();
        }

        private void addNewItemButton_Click(object sender, RoutedEventArgs e)
        {
            AddNewItems openNewitems = new AddNewItems();
            openNewitems.Show();
            

        }
    }
}
