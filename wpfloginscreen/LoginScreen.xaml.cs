﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;


namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        public static User currentUser { get; set; }
        public static DBCon dbCon { get; set; }

        public LoginScreen() 
        {
            InitializeComponent();
            txtUsername.Focus();

        }

        private void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {

            string aCon = "Data Source=.;Initial Catalog=LogonDB;Integrated Security=True;Connect Timeout=1";
            string vCon = "Data Source=.\\SQLEXPRESS;Initial Catalog=LogonDB;Integrated Security=True;Connect Timeout=1";
            
            // instantiate the sql connection object aswell as check for a valid db connection string
            
            dbCon = new DBCon(aCon, vCon);
            SqlConnection sqlCon = dbCon.SQLConnection();

            // qol, if password field is empty give a specific message

            if (txtPassword.Text == null || txtPassword.Text == String.Empty)
            { 
                System.Windows.MessageBox.Show("Please enter a password.");
            }
            else
            {
                try
                {
                    if (sqlCon.State == ConnectionState.Closed)
                    {
                        sqlCon.Open();
                    }

                    // query: select the row where input matches data
                    String query = "SELECT * FROM tbluser WHERE UserID=@UserID AND Password=@Password";

                    // instantiate a new sqlcommand object
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);

                    // userinput to query
                    sqlCmd.Parameters.AddWithValue("@UserID", txtUsername.Text);
                    sqlCmd.Parameters.AddWithValue("@Password", txtPassword.Password);

                    // true if username and password matches an entry in the database
                    bool inputCorrect = Convert.ToBoolean(sqlCmd.ExecuteScalar());

                    if (inputCorrect)
                    {

                        // initialize local variables that takes information from db
                        string UID = "", UName = "", UPassword = "";
                        decimal HoursWorked = 0;

                        using (SqlDataReader reader = sqlCmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                UID = reader["UserID"].ToString();
                                UName = reader["Username"].ToString();
                                UPassword = reader["Password"].ToString();
                                HoursWorked = Convert.ToDecimal(reader["fldHoursWorked"].ToString());
                                //registeredDate = Convert.ToDateTime(reader["fldRegisteredDate"].ToString());
                            }
                        }
                        //User currentUser = new User();
                        currentUser = new User(UID, UName, UPassword, HoursWorked);
                        currentUser.Start = DateTime.Now;

                        System.Windows.MessageBox.Show("Welcome " + currentUser.UName + ".");

                        // there is probably a better way to do this
                        string admin = "5";
                        string sales = "3";
                        string cashier = "2";

                        if (currentUser.UID.Substring(0, 1) == admin && currentUser.UID.Length >= 2)
                        {
                            //admin -> admin window
                            AdminWindow adm = new AdminWindow();
                            adm.Show();
                            this.Close();
                        }

                        else if (currentUser.UID.Substring(0, 1) == cashier && currentUser.UID.Length >= 2)
                        {
                            //cashier -> cashier window
                            CashierWindow cash = new CashierWindow();                        
                            cash.Show();
                            this.Close();

                        }

                        else if (currentUser.UID.Substring(0, 1) == sales && currentUser.UID.Length >= 2)
                        {
                            //sales -> report window (NOT YET PROPERLY IMPLEMENTED)
                            AdminWindow dashboard = new  AdminWindow ();
                            dashboard.Show();
                            this.Close();
                        }
                        else
                        {
                            System.Windows.MessageBox.Show("Something is wrong! (possibly a non 3 digit entry in database)");
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Username or password is incorrect.");
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
                finally
                {
                    sqlCon.Close();
                }
            }
        }
    }
}
