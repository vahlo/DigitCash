﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace wpfloginscreen
{
    public class DBCon 
    {
        public string ACon { get; set; }
        public string VCon { get; set; }
        public SqlConnection SQLCon { get; set; }

        public DBCon() { }

        public DBCon(string aCon, string vCon)
        {
            ACon = aCon;
            VCon = vCon;
        }
        
        public SqlConnection SQLConnection()
        {
            string conString;

            if (this.CanConnect(VCon))
            {
                conString = VCon;
            }
            else
            {
                conString = ACon;
            }

            SQLCon = new SqlConnection(conString);
            return SQLCon;
        }

        private bool CanConnect(string connectionString)
        {
            try
            {
                using (SqlConnection testConnection = new SqlConnection(connectionString))
                {
                    testConnection.Open();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
