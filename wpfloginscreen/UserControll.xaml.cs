﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for UserControll.xaml
    /// </summary>
    public partial class UserControll : Window
    {

        SqlCommand sqlCmd;
        String query = "SELECT UserID, Username FROM tbluser ORDER BY UserID ASC";
        DataSet dtSet = new DataSet();
        public static String oldName;
 

        public UserControll()
        {
            InitializeComponent();
            BindData();
            itemSearchBox.Focus();
        }

        void WindowActive(object sender, EventArgs e)
        {
            // Application activated
            BindData();
        }

        void WindowNotActive(object sender, EventArgs e)
        {
            // Application deactivated
            //this.isActive = false;
        }


        private void BindData()
        {
            using (SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection())
            {
                dtSet.Clear();
                sqlCmd = new SqlCommand(query, sqlCon);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtSet, "tbluser");
                ItemList.DataContext = dtSet;
            }
        }

        private void itemSearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (itemSearchBox.Text.Length < 1)
            {
                ItemList.DataContext = dtSet;
            }
            else
            {
                DataSet tempData = dtSet;
                DataSet newdataset = new DataSet();
                string searchString = "Username Like '%" + itemSearchBox.Text + "%'";
                string orderString = "UserID ASC";
                newdataset.Merge(tempData.Tables[0].Select(searchString, orderString));
                ItemList.DataContext = newdataset;
            }

        }

        private void ItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView drv = (DataRowView)ItemList.SelectedItem;

            string searchID;
            if (drv != null)
            {
                searchID = drv["UserID"].ToString().Trim();

                SqlCommand searchSqlCmd;
                string searchQuery = "SELECT * from tbluser WHERE UserID = " + searchID;
                using (SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection())
                {
                    if (sqlCon.State == ConnectionState.Closed)
                    {
                        sqlCon.Open();
                    }

                    searchSqlCmd = new SqlCommand(searchQuery, sqlCon);
                    using (SqlDataReader reader = searchSqlCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            UserIDBox.Text = reader["UserID"].ToString();
                            UsernameBox.Text = reader["Username"].ToString();
                            PasswordBox.Password = reader["Password"].ToString();
                            hoursWorkedBox.Text = reader["fldHoursWorked"].ToString();
                        }
                    }
                    sqlCon.Close();
                }
                 oldName = UsernameBox.Text;
            }
        }
    
        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            signup openSignup = new signup();
            openSignup.Show();
        }


        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();
            String query = "DELETE FROM tbluser Where UserID=" + "'" + UserIDBox.Text + "'";

            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }

                if (MessageBox.Show("Are you sure that you want to delete user " + UsernameBox.Text + " from the system?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes)

                {
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.ExecuteNonQuery();

                    MessageBox.Show("User " + UsernameBox.Text + " has successfully been deleted.");
                    BindData();
                }
                else
                {
                    return;
                }
            }

            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
            finally
            {
                sqlCon.Close();
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {

            SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();
            String query = "Update tbluser set Username='" + UsernameBox.Text + "',Password='" + PasswordBox.Password + "', fldHoursWorked='" + hoursWorkedBox.Text + "' where UserID=" + "'" + UserIDBox.Text + "'";
            

            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }

                if (MessageBox.Show(" Are you Sure that you want to update user " + oldName + "?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes)

                {
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.ExecuteNonQuery();

                    MessageBox.Show("The User " + "" + oldName + " has successfully been Updated.");
                    BindData();
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
            finally
            {
                sqlCon.Close();
            }
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {

            AdminWindow openAdmin = new AdminWindow();
            openAdmin.Show();
            this.Close();

        }
    }
}


