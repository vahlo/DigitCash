﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for AddNewItems.xaml
    /// </summary>
    public partial class AddNewItems : Window
    {
        public AddNewItems()
        {
            InitializeComponent();

        }


        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            //Sätter värden på objectet newItem
            Item newItem = new Item();
            newItem.IName = NameBox.Text.Trim();
            newItem.IStock = Convert.ToInt32(StockBox.Text.Trim());
            newItem.IPrice = Convert.ToDecimal(PriceBox.Text.Trim());
            //Kilo price checkbox
            if (WeightPriceBox.IsChecked == true)
            {
                newItem.PerItem = false;
                newItem.IName +=  " (kg)";
                    
            }
            else { newItem.PerItem = true; }

            //Radiobuttons VAT Check

            if (sixVatBox.IsChecked == true) { newItem.IVat = Convert.ToDecimal(0.06); }
            else if (twelveVatBox.IsChecked == true) { newItem.IVat = Convert.ToDecimal(0.12); }
            else if (twoFiveVatBox.IsChecked == true) { newItem.IVat = Convert.ToDecimal(0.25); }



            SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();
            {
                sqlCon.Open();
            }

            //Läser in högsta itemID  från db och +1

            string query1 = "SELECT MAX(itemID) FROM inventory";
            SqlCommand sqlCmd1 = new SqlCommand(query1, sqlCon);
            using (SqlDataReader read = sqlCmd1.ExecuteReader())
            {
                while (read.Read())
                {
                        int newItemID = Convert.ToInt32(read[""].ToString());
                        newItemID = newItemID + 1;
                        newItem.IID = newItemID;
                    }
            }

            //SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();
            //{
            //    sqlCon.Open();
            //}



            //"spitem" är query för att lägga till ny item i  vårt table
            try { 
            SqlCommand sqlCmd = new SqlCommand("spItemAdd", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;


            //Lägger in värderna från användaren till rätt positioner i tabelen

            sqlCmd.Parameters.AddWithValue("@itemID", newItem.IID); //Trim tar bort mellanslag i början evt slutet
            sqlCmd.Parameters.AddWithValue("@name", newItem.IName);
            sqlCmd.Parameters.AddWithValue("@stock", newItem.IStock);
            sqlCmd.Parameters.AddWithValue("@price", newItem.IPrice);
            sqlCmd.Parameters.AddWithValue("@perUnit", newItem.PerItem);
            sqlCmd.Parameters.AddWithValue("@fldVAT", newItem.IVat);
            sqlCmd.ExecuteNonQuery(); //exicute ovanstående
            MessageBox.Show("Registration is Sucessfull");
            Clear();
            }

            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
            finally
            {
                sqlCon.Close();
            }

        }

        void Clear()
        {
            NameBox.Text = StockBox.Text = PriceBox.Text = "";
        }

        private void BackButtonItem_Click(object sender, RoutedEventArgs e)
        {
            
            this.Close();
        }
    }




}

     
    
