﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;

namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for CashierWindow.xaml
    /// </summary>
 
    public partial class CashierWindow : Window
    {
        // shopping cart as list, public static to be accessible from other windows (receipt, checkout?).
        public static List<List<object>> cart { get; set; }

        // instance objects and variables for our search function
        SqlCommand sqlCmd;
        String searchQuery = "SELECT itemID, name FROM inventory ORDER BY itemID ASC";
        DataSet dtSet = new DataSet();
        static int i = 0;
        //static int n = 0;

        public CashierWindow()
        {
            InitializeComponent();
            itemIDBox.Focus();

            // clear cart from previous sessions
            if (cart != null)
            {
                cart.Clear();
                i = 0;
            }

            // initiate the shopping cart
            cart = new List<List<object>>();

            // fill search box with items
            BindData();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_logout(object sender, RoutedEventArgs e)
        {
            // timelog as per defined in our User class.
            LoginScreen.currentUser.TimeLog();

            //System.Windows.MessageBox.Show("You've worked for a total of " + LoginScreen.currentUser.HoursWorked.ToString("0.00") + " hours."
            //        + "\nGive yourself a pat on the back.");

            LoginScreen logOut = new LoginScreen();
            logOut.Show();
            this.Close();
        }

        private void Button_Click_0(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button0.Content;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button1.Content;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button2.Content;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button3.Content;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button4.Content;
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button5.Content;
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button6.Content;
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button7.Content;
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button8.Content;
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += button9.Content;
        }

        private void Button_Click_add(object sender, RoutedEventArgs e)
        {
            
            SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();

            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }

                // fetch the first row where itemID matches database entry
                String query = "SELECT * FROM inventory WHERE itemID=@itemID";

                // initiate our SqlCommand object and define it as a manual query type
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon)
                {
                    CommandType = CommandType.Text
                };

                // here is where we fetch the itemID from the textbox
                string item = itemIDBox.Text;
                string mult = "1";

                // if adding multiple items the itemID becomes the substring before the '*' symbol
                // while the multiplier is the substring after the '*' symbol
                // also, replace comma with dot to avoid syntax errors on numpad when adding 
                // per weight items
                if (itemIDBox.Text.Contains('*'))
                {
                    item = itemIDBox.Text.Substring(0, itemIDBox.Text.IndexOf('*'));
                    if (itemIDBox.Text.Contains(","))
                    {
                        itemIDBox.Text = itemIDBox.Text.Replace(",", ".");
                    }
                    mult = itemIDBox.Text.Substring(itemIDBox.Text.LastIndexOf("*") + 1);
                }

                // add parameter to our Sqlcommand
                sqlCmd.Parameters.AddWithValue("@itemID", item);

                double price = 0;

                //==================================================
                // this is where we read from our database to collect information about the added item
                // and adds it to a shopping cart consisting of nested lists.
                // it takes the perUnit boolean in the inventory into consideration and prompts the user with
                // a message asking them to fill in weight.
                //==================================================
                using (SqlDataReader read = sqlCmd.ExecuteReader())
                {
                    if (read.HasRows)
                    {
                        while (read.Read())
                        {
                            if (!Convert.ToBoolean(read["perUnit"].ToString()) && !itemIDBox.Text.Contains('*'))
                            {
                                MessageBox.Show("Please enter the weight of selected item.");
                                itemIDBox.Text += "*";
                                itemIDBox.SelectionStart = itemIDBox.Text.Length;
                                break;
                            }
                            else if (item.Equals(read["itemID"].ToString().Trim()))
                            {
                                if (itemIDBox.Text.Contains('.') && Convert.ToBoolean(read["perUnit"].ToString()))
                                {
                                    MessageBox.Show("Invalid character used. '.'");
                                    break;
                                }
                                if (!Convert.ToBoolean(read["perUnit"].ToString()))
                                {
                                    addedBox.AppendText((mult + " kg").PadRight(7) + read["name"].ToString().PadRight(13) + ((Decimal.Parse(read["price"].ToString()) * Decimal.Parse(mult)).ToString("0.00")).PadLeft(7) + " kr\n");
                                }
                                else
                                    addedBox.AppendText((mult + " st").PadRight(7) + read["name"].ToString().PadRight(13) + ((Decimal.Parse(read["price"].ToString()) * Decimal.Parse(mult)).ToString("0.00")).PadLeft(7) + " kr\n");

                                price = double.Parse(read["price"].ToString()) * double.Parse(mult);
                                //==================================================
                                // our shopping cart list gets filled with all unique 
                                // items added by the cashier.
                                // any duplicate items adds to the multiplier.
                                //==================================================
                                cart.Add(new List<object>());
                                cart[i].Add(read["name"].ToString().Trim());
                                cart[i].Add(mult);
                                cart[i].Add(price);
                                cart[i].Add(double.Parse(read["fldVAT"].ToString().Trim()));

                                for (int j = 0; j < cart.Count - 1; j++)
                                {
                                    if (read["name"].ToString().Trim().Equals(cart[j][0].ToString()))
                                    {
                                        cart[j][1] = (Decimal.Parse(cart[j][1].ToString()) + Decimal.Parse(mult)).ToString().Trim();
                                        cart[j][2] = (double.Parse(cart[j][2].ToString()) + price).ToString();
                                        double vat = Convert.ToDouble(cart[j][3].ToString());
                                        //cart[j][3] = vat *= price;
                                        cart.RemoveRange(cart.Count - 1, 1);
                                        i--;
                                    }
                                }
                                i++;

                                // qol scroll and fill the total price textbox, aswell as clearing the itemIDbox
                                itemIDBox.Text = "";
                                addedBox.ScrollToEnd();
                                double currentTotal = double.Parse(totalBox.Text.Substring(0, totalBox.Text.IndexOf(' '))) + price;
                                totalBox.Text = currentTotal.ToString();
                                totalBox.AppendText(" kr");
                            }
                            itemIDBox.Text = "";
                        }
                    }
                    else
                    {
                        MessageBox.Show("No item with itemID: " + item + " in database.");
                        itemIDBox.Text = "";
                    }
                }
            }

            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
            finally
            {

                sqlCon.Close();
            }
        }

        private void Button_Click_mult(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += "*";

        }

        private void Button_Click_coupon(object sender, RoutedEventArgs e)
        {
            var dialog = new Coupon();
            if (dialog.ShowDialog() == true)
            {

                addedBox.AppendText("1 st".PadRight(7) + "Coupon/voucher".PadRight(13) + (Convert.ToDouble(dialog.ResponseText) * (-1.0)).ToString().PadLeft(7) + " kr\n");
                cart.Add(new List<object>());
                cart[i].Add("Coupon/voucher");
                cart[i].Add(1);
                cart[i].Add(Convert.ToDouble(dialog.ResponseText)*(-1.0));
                cart[i].Add(0);
                i++;

                addedBox.ScrollToEnd();
                double currentTotal = double.Parse(totalBox.Text.Substring(0, totalBox.Text.IndexOf(' '))) + Convert.ToDouble(dialog.ResponseText) * (-1.0);
                totalBox.Text = currentTotal.ToString();
                totalBox.AppendText(" kr");
                itemIDBox.Text = "";
                itemIDBox.Focus();
            }
        }

        private void Button_Click_checkout(object sender, RoutedEventArgs e)
        {
            if (cart.Count > 0)
            {
                SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }



                Receipt newReceipt = new Receipt();
                newReceipt.Show();
            }
        }

        private void Button_Click_cancel(object sender, RoutedEventArgs e)
        {
            itemIDBox.Clear();
            itemIDBox.Focus();
        }
        private void Button_Click_panic(object sender, RoutedEventArgs e)
        {
            itemIDBox.Clear();
            addedBox.Clear();
            itemSearchBox.Clear();

            cart.Clear();
            i = 0;

            totalBox.Text = "0 kr";
            itemIDBox.Focus();
        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void remove_Click(object sender, RoutedEventArgs e)
        {
            if (itemIDBox.Text.Length > 0)
                itemIDBox.Text = itemIDBox.Text.Remove(itemIDBox.Text.Length - 1);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CashierSearch CSearch =  new CashierSearch() { Owner = this };
            CSearch.Show();
        }

        private void Button_Click_comma(object sender, RoutedEventArgs e)
        {
            itemIDBox.Text += ".";

        }


        // this method fills a dataset with entries from the inventory table
        // and fills an itemlist with them
        private void BindData()
        {
            using (SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection())
            {

                sqlCmd = new SqlCommand(searchQuery, sqlCon);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtSet, "inventory");
                ItemList.DataContext = dtSet;
            }
        }

        // this method acts as a dynamic search function, filling a dataset with 
        // the intersection of our original set and the entries that fullfil our search string
        private void itemSearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (itemSearchBox.Text.Length < 1)
            {
                ItemList.DataContext = dtSet;
            }

            DataSet tempData = dtSet;
            DataSet newdataset = new DataSet();

            string selectString = "name Like '%" + itemSearchBox.Text + "%'";
            string orderString = "itemID ASC";
            newdataset.Merge(tempData.Tables[0].Select(selectString, orderString));
            ItemList.DataContext = newdataset;
        }

        private void ItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView drv = (DataRowView)ItemList.SelectedItem;

            if (drv != null)
                /*cashierwindow.*/itemIDBox.Text = drv["itemID"].ToString().Trim();
            itemIDBox.Focus();
            itemIDBox.SelectionStart = itemIDBox.Text.Length;
        }
    }
}
