﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for Receipt.xaml
    /// </summary>
    public partial class Receipt : Window
    {
        public int ReceiptID { get; set; }
        public DateTime PurchaseDT { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalVat { get; set; }
        static int n = 0;

        string row;

        string receiptString = "         ___ ___ ___ ___ _____           \n" +
                                "        |   \\_ _/ __|_ _|_   _|        \n" +
                                "        | |) | | (_ || |  | |           \n" +
                                "     ___|___/___\\___|___|_|_|__ ___    \n" +
                                "    / __| /_\\ / __| || |_ _| __| _ \\  \n" +
                                "   | (__ / _ \\ __ \\ __ || || _||   /  \n" +
                                "    \\___/_/ \\_\\___/_||_|___|___|_|_\\\n" +
                                "    =================================   \n" +
                                "          " + DateTime.Now.ToString() + "\n\n" +
                                " ID              n      a         Price \n" +
                                " -------------------------------------- \n";

        ShoppingCart CART = new ShoppingCart(CashierWindow.cart);

        public Receipt()
        {
            InitializeComponent();

            // Time of purchase aswell as price- and VAT calculation
            PurchaseDT = DateTime.Now;
            PurchaseDT = PurchaseDT.AddTicks(-(PurchaseDT.Ticks % TimeSpan.TicksPerSecond));
            TotalPrice = Convert.ToDecimal(CART.TotalPrice.ToString("0.00"));
            TotalVat = Convert.ToDecimal(CART.VatPrice.ToString("0.00"));

            SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();

            if (sqlCon.State == ConnectionState.Closed)
            {
                sqlCon.Open();
            }

            // read the highest receiptID from the tblReceipt and store a new int 
            // with +1 to be used in our next receipt
            string queryRead = "SELECT MAX(fldReceiptID) FROM tblReceipt";
            SqlCommand sqlCmdRead = new SqlCommand(queryRead, sqlCon);

            using (SqlDataReader read = sqlCmdRead.ExecuteReader())
            {
                while (read.Read())
                {
                    ReceiptID = Convert.ToInt32(read[""].ToString()) + 1;
                }
            }
            
            // create a new receipt entry in our tblReceipt table, storing ID, time of purchase, amount and VAT
            string queryWrite = "INSERT into tblReceipt(fldReceiptID,fldDateTime,fldAmount,fldVat) values(@fldReceiptID, @fldDateTime, @fldAmount, @fldVat)";

            SqlCommand sqlCmdWrite = new SqlCommand(queryWrite, sqlCon);
            sqlCmdWrite.CommandType = CommandType.Text;

            sqlCmdWrite.Parameters.AddWithValue("@fldReceiptID", ReceiptID);
            sqlCmdWrite.Parameters.AddWithValue("@fldDateTime", PurchaseDT);
            sqlCmdWrite.Parameters.AddWithValue("@fldAmount", TotalPrice);
            sqlCmdWrite.Parameters.AddWithValue("@fldVat", TotalVat);

            sqlCmdWrite.ExecuteNonQuery();

            // =========================================================================================
            // The following codeblock updates our inventory table to remove the sold items from stock
            // aswell as updating the sold items table with new information
            // using a transaction query to run both procedures in one loop
            // =========================================================================================

            foreach (var sublist in CashierWindow.cart)
            {
                if (CashierWindow.cart[n][0].ToString() != "Coupon/voucher")
                {
                    string query = "BEGIN TRANSACTION; " +
                                   "UPDATE inventory " +
                                   "SET stock = stock -" + CashierWindow.cart[n][1] +
                                   " WHERE name='" + CashierWindow.cart[n][0] + "'; " +

                                   "INSERT INTO tblSoldItems(fldReceiptID, fldItemName, fldAmountSold) " +
                                   "SELECT R.fldReceiptID, '" + CashierWindow.cart[n][0] + "', " + CashierWindow.cart[n][1] +
                                   "FROM tblReceipt R " +
                                   "WHERE R.fldReceiptID = " + ReceiptID + "; " +
                                   "COMMIT;";

                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.CommandType = CommandType.Text;

                    sqlCmd.ExecuteNonQuery();
                }
                n++;
            }
            

            // print the text onto our receipt textbox
            receiptBox.Text = receiptString;
            List<object> rowlist = new List<object> { };

            // iterating over every item in our shopping cart
            foreach (var sublist in CashierWindow.cart)
            {
                foreach (var value in sublist)
                {
                    rowlist.Add(value);
                }
                row = " " + rowlist[0].ToString().PadRight(16) + rowlist[1].ToString().PadRight(3) +
                   (Convert.ToDouble(rowlist[2].ToString())/Convert.ToDouble(rowlist[1].ToString())).ToString().PadLeft(5) + " kr" + rowlist[2].ToString().PadLeft(8) + " kr\n";
                receiptBox.AppendText(row);
                rowlist.Clear();
            }
            receiptBox.AppendText(" --------------------------------------\n");
            receiptBox.AppendText("\n Sub total: " + (CART.TotalPrice - CART.VatPrice).ToString("0.00") + " kr");
            receiptBox.AppendText("\n Tax: " + CART.VatPrice.ToString("0.00") + " kr");
            receiptBox.AppendText("\n Total: " + CART.TotalPrice.ToString("0.00") + " kr\n");
            receiptBox.AppendText("\n Your cashier was: " + LoginScreen.currentUser.UName);
            receiptBox.AppendText("\n Receipt ID: " + ReceiptID);
            receiptBox.AppendText("\n\n Digit Cashier");
            receiptBox.AppendText("\n Org. nummer: 123");

            sqlCon.Close();
            n = 0;
        }

        private void receiptBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            // if for some reason you want to store the receipt locally we have this method 
            // to create a comma separated textfile containing data from the purchase
            using (StreamWriter outfile = new StreamWriter(@"C:\DigitCashier\Receipts\" + string.Format("kvitto-{0:yyyy-MM-dd_HH-mm-ss}.txt", DateTime.Now), false))
            {
                string content = "";
                foreach (var sublist in CashierWindow.cart)
                {
                    foreach (var value in sublist)
                    {
                        content += value + ", ";
                    }
                    if (content.Length > 1)
                        content = content.TrimEnd().Substring(0, content.Length - 2);
                    outfile.WriteLine(content);
                    content = "";
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
