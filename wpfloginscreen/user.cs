﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace wpfloginscreen
{
    public class User
    {
        public string UID { get; set; }
        public string UName { get; set; }
        public string UPassword { get; set; }
        public decimal HoursWorked { get; set; }
        public DateTime RegisteredDate { get; set; }
        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }

        public User() { }

        public User(string uID, string uName, string uPassword, decimal hoursWorked)
        {
            UID = uID;
            UName = uName;
            UPassword = uPassword;
            HoursWorked = hoursWorked;
        }
        
        public User(string uID, string uName, string uPassword, decimal hoursWorked, DateTime regDate)
        {
            UID = uID;
            UName = uName;
            UPassword = uPassword;
            HoursWorked = hoursWorked;
            RegisteredDate = regDate;
        }

        public void TimeLog()
        {
            this.Stop = DateTime.Now;
            decimal span = Convert.ToDecimal((this.Stop - this.Start).TotalHours);
            this.HoursWorked += span;

            string timelogQuery = "UPDATE tbluser SET fldHoursWorked=@fldHoursWorked WHERE UserID='" + this.UID.ToString() + "'";
            SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();
            if (sqlCon.State == ConnectionState.Closed)
            {
                sqlCon.Open();
            }

            SqlCommand sqlCmd = new SqlCommand(timelogQuery, sqlCon)
            {
                CommandType = CommandType.Text
            };
            sqlCmd.Parameters.AddWithValue("@fldHoursWorked", this.HoursWorked.ToString("0.00"));
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();
        }




        class Cashier : User
        {

        }

        class Boss : User
        {

        }

        class Admin : User
        {

        }
    }
}
