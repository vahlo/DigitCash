﻿namespace wpfloginscreen
{
    partial class SalesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.LogonDBDataSet1 = new wpfloginscreen.LogonDBDataSet1();
            this.tblSoldItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tblSoldItemsTableAdapter = new wpfloginscreen.LogonDBDataSet1TableAdapters.tblSoldItemsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.LogonDBDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSoldItemsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.tblSoldItemsBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "wpfloginscreen.SalesReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(12, 8);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(1272, 843);
            this.reportViewer1.TabIndex = 0;
            // 
            // LogonDBDataSet1
            // 
            this.LogonDBDataSet1.DataSetName = "LogonDBDataSet1";
            this.LogonDBDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblSoldItemsBindingSource
            // 
            this.tblSoldItemsBindingSource.DataMember = "tblSoldItems";
            this.tblSoldItemsBindingSource.DataSource = this.LogonDBDataSet1;
            // 
            // tblSoldItemsTableAdapter
            // 
            this.tblSoldItemsTableAdapter.ClearBeforeFill = true;
            // 
            // SalesReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1296, 849);
            this.Controls.Add(this.reportViewer1);
            this.Name = "SalesReport";
            this.Text = "SalesReport";
            this.Load += new System.EventHandler(this.SalesReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LogonDBDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSoldItemsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource tblSoldItemsBindingSource;
        private LogonDBDataSet1 LogonDBDataSet1;
        private LogonDBDataSet1TableAdapters.tblSoldItemsTableAdapter tblSoldItemsTableAdapter;
    }
}