﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//Testkomentar
namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        public AdminWindow()
        {
            InitializeComponent();
        }
        private void Button_Click_logout(object sender, RoutedEventArgs e)
        {
            LoginScreen.currentUser.TimeLog();
            LoginScreen logOut = new LoginScreen();
            logOut.Show();
            this.Close();
        }

        private void User_Click(object sender, RoutedEventArgs e)
        {

            UserControll openUsers = new UserControll();
            openUsers.Show();
            this.Close();
        }

        private void inventory_Click(object sender, RoutedEventArgs e)
        {
            InventoryControll openinventory = new InventoryControll();
            openinventory.Show();
            this.Close();
        }

        private void ReportButton_Click(object sender, RoutedEventArgs e)
        {
            HoursReportForm rp = new HoursReportForm();
            rp.Show();
           // this.Close();
        }

        private void SalesReportButton_Click(object sender, RoutedEventArgs e)
        {
            SalesReport rs = new SalesReport();
            rs.Show();
        }

        private void UserReportButton_Click(object sender, RoutedEventArgs e)
        {
            HoursReportForm rp = new HoursReportForm();
            rp.Show();
        }

        private void InventoryStatusButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
