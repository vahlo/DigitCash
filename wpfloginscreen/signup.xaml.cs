﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for signup.xaml
    /// </summary>
    public partial class signup : Window
    {
        public signup()
        {
            InitializeComponent();
            
        }
        
        

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {

           

            //test av radio button admin
            


           // txtUserID.Text.Trim(), txtUsername.Text.Trim(), txtPassword.Password.Trim(), 0, DateTime.Now
            User newUser = new User();
            newUser.UName = txtUsername.Text.Trim();
            newUser.UPassword = txtPassword.Password.Trim();
            newUser.HoursWorked = 0;
            newUser.RegisteredDate = DateTime.Now;

            //Kopplingen till (Viktors) SQL Server
            //SqlConnection sqlCon = new SqlConnection("Data Source=VIKTORKLLBOAA42\\SQLEXPRESS;Initial Catalog=LogonDB;Integrated Security=True");
            SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection();
            {
                sqlCon.Open();


                //Kollar vilken av 3 radiobuttons är =1, sätter läser in högsta userID och +1, sätter sedan det till användaren nya userid
                if (CashierCheck.IsChecked == true)
                {
                    SqlCommand sqlCmd1 = new SqlCommand("spCasherCheck", sqlCon);
                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader read = sqlCmd1.ExecuteReader())
                    {
                        while (read.Read())
                        {
                            int newUserID = Convert.ToInt32(read[""].ToString());
                            newUserID = newUserID + 1;
                            newUser.UID = Convert.ToString(newUserID);
                        }
                    }
                }

                else if (SalesCheck.IsChecked == true)
                {
                    SqlCommand sqlCmd1 = new SqlCommand("spSalesCheck", sqlCon);
                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader read = sqlCmd1.ExecuteReader())
                    {
                        while (read.Read())
                        {
                            int newUserID = Convert.ToInt32(read[""].ToString());
                            newUserID = newUserID + 1;
                            newUser.UID = Convert.ToString(newUserID);
                        }
                    }
                }

                else if (AdminCheck.IsChecked == true)
                {
                    SqlCommand sqlCmd1 = new SqlCommand("spAdminCheck", sqlCon);
                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader read = sqlCmd1.ExecuteReader())
                    {
                        while (read.Read())
                        {
                            int newUserID = Convert.ToInt32(read[""].ToString());
                            newUserID = newUserID + 1;
                            newUser.UID = Convert.ToString(newUserID);
                        }
                    }
                }


                //Signup bug checks
                //Om man ej väljer någon radiobutton skickar den error och skickar den tillbaka till Signup
                //RadioButton rd = sender as RadioButton;
                //if ( rd = checked ) { MessageBox.Show("You need to select a Role for the User"); return; }


                //Min 3 characters in a password check
                if (newUser.UPassword.Length < 3) { MessageBox.Show("Password needs to be longer then 3 characters."); return; }
                

                //"Useradd" är query för att lägga till ny person i  vårt table
                SqlCommand sqlCmd = new SqlCommand("UserAdd", sqlCon);
                sqlCmd.CommandType = CommandType.StoredProcedure;


                //Lägger in värderna från användaren till rätt positioner i tabelen
                
                sqlCmd.Parameters.AddWithValue("@UserID", newUser.UID); //Trim tar bort mellanslag i början evt slutet
                sqlCmd.Parameters.AddWithValue("@Username", newUser.UName);
                sqlCmd.Parameters.AddWithValue("@Password", newUser.UPassword);
                sqlCmd.Parameters.AddWithValue("@fldHoursWorked", newUser.HoursWorked);
                sqlCmd.Parameters.AddWithValue("@fldRegisteredDate", newUser.RegisteredDate);
                sqlCmd.ExecuteNonQuery(); //exicute ovanstående
                MessageBox.Show("Registration is Sucessfull");
                Clear();

            }

            void Clear()
            {
                txtUsername.Text = txtPassword.Password = "";
            }

        }
        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            this.Close();
        }
    }
}
