﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpfloginscreen
{

    class ShoppingCart
    {
        public List<List<Object>> Cart { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal VatPrice { get; set; }

        public ShoppingCart() { }

        public ShoppingCart(List<List<Object>> cart)
        {
            Cart = cart;
            TotalPrice = this.CalculatePrice();
            VatPrice = this.CalculateVAT();
        }

        public decimal CalculatePrice()
        {
            List<List<Object>> cart = this.Cart;
            decimal price = 0;
            for (int i = 0; i < cart.Count; i++)
            {
                price += decimal.Parse(cart[i][2].ToString());
            }
            return price;
        }
        public decimal CalculatePrice(List<List<Object>> cart)
        {
            decimal price = 0;
            for (int i = 0; i < cart.Count; i++)
            {
                price += decimal.Parse(cart[i][2].ToString());
            }
            return price;
        }
        public decimal CalculateVAT()
        {
            List<List<Object>> cart = this.Cart;
            decimal VatPrice = 0;
            for (int i = 0; i < cart.Count; i++)
            {
                VatPrice += Convert.ToDecimal(double.Parse(cart[i][3].ToString())*double.Parse(cart[i][2].ToString()));
            }
            return VatPrice;
        }

        public decimal CalculateVAT(List<List<Object>> cart)
        {
            decimal VatPrice = 0;
            for (int i = 0; i < cart.Count; i++)
            {
                VatPrice += Convert.ToDecimal(double.Parse(cart[i][3].ToString()) * double.Parse(cart[i][2].ToString()));
            }
            return VatPrice;
        }
    }
}
