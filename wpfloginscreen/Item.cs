﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpfloginscreen
{
    class Item
    {
        public int IID { get; set; }
        public string IName { get; set; }
        public int ICategory { get; set; }
        public int IStock { get; set; }
        public decimal IPrice { get; set; }
        public bool PerItem { get; set; }
        public decimal IVat { get; set; }

        public Item() { }

        public Item(int itemID, string itemName, int itemCategory, int itemStock, decimal itemPrice, bool perItem, decimal vat)
        {
            IID = itemID;
            IName = itemName;
            ICategory = itemCategory;
            IStock = itemStock;
            IPrice = itemPrice;
            PerItem = perItem;
            IVat = vat;

        }
    }
}
