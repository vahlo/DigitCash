﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Xceed.Wpf.Toolkit;

namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for CashierSearch.xaml
    /// </summary>
    public partial class CashierSearch : Window
    {
        SqlCommand sqlCmd;
        String query = "SELECT itemID, name FROM inventory ORDER BY itemID ASC";
        DataSet dtSet = new DataSet();

        public CashierSearch()
        {
            InitializeComponent();
            BindData();
        }

        private void BindData()
        {
            using (SqlConnection sqlCon = LoginScreen.dbCon.SQLConnection())
            {

                sqlCmd = new SqlCommand(query, sqlCon);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dtSet, "inventory");
                ItemList.DataContext = dtSet;
            }
        }

        private void itemSearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (itemSearchBox.Text.Length < 1)
            {
                ItemList.DataContext = dtSet;
            }

            DataSet tempData = dtSet;
            DataSet newdataset = new DataSet();
            string searchString = "name Like '%" + itemSearchBox.Text + "%'";
            newdataset.Merge(tempData.Tables[0].Select(searchString));
            ItemList.DataContext = newdataset;
        }

        private void ItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView drv = (DataRowView)ItemList.SelectedItem;
            CashierWindow cashierwindow = Owner as CashierWindow;
            cashierwindow.itemIDBox.Clear();

            if (drv != null)
                cashierwindow.itemIDBox.Text = drv["itemID"].ToString().Trim();
        }
    }
}
