﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace wpfloginscreen
{
    /// <summary>
    /// Interaction logic for Coupon.xaml
    /// </summary>
    public partial class Coupon : Window
    {

        public Coupon()
        {
            InitializeComponent();
            CouponAmountBox.Focus();
        }

        public string ResponseText
        {
            get { return CouponAmountBox.Text; }
            set { CouponAmountBox.Text = value; }
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            
            DialogResult = true;
        }
    }
}
